// ==UserScript==
// @name         Masto Concentration
// @version      1.0
// @description  Rend les colonnes invisibles, sauf si la souris passe dessus
// @author       Lapineige
// @match        *://mamot.fr/*
// il vous faut adapter le nom de domaine à celui de votre instance // change it to match your instance URL

// @grant        GM_addStyle
// ==/UserScript==

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}


///// Paramètres / parameters
addGlobalStyle(':root { --opacity-hidden: 0.05 ;}') // régler l'opacité du contenu masqué ici   // adjust opacity


addGlobalStyle(':root { --transition-time-in: .5s ;}') // régler le temps de transition ici. // sens: transparent -> visible
addGlobalStyle(':root { --transition-time-out: 1s ;}') // régler le temps de transition ici. // sens: visible -> transparent

/////// 3 options disponibles   // 3 options available
//////// 1 - N'afficher que la colonne survolée en cours + celle d'écriture     // focused column + drawer
//addGlobalStyle('.column:not(:hover) {transition: opacity ease-in var(--transition-time-in); opacity: var(--opacity-hidden);}');

//////// 2 - Quand on rédige : cacher tout sauf notifications + fenêtre de rédaction    // notifications column + drawer
addGlobalStyle('.drawer:hover ~.column {transition: opacity ease-in var(--transition-time-in); opacity: var(--opacity-hidden);}');

//////// 3 - Quand on rédige : cacher tout sauf Notifications + Fenêtre de rédaction + colonne du pouet/profil sélectionné      // drawer + notification + selected toot/profile
//// note importante : l'outil ci-dessous n'est pas encore supporté par défaut dans Firefox (https://caniuse.com/?search=has) -> en attendant, il est activable dans la page about:config, en mettant layout.css.has-selector.enabled à true.
//// important note : the tool used below is not yet supported by default in Firefox (https://caniuse.com/?search=has) -> meanwhile, you can activate it in about:config page, by setting layout.css.has-selector.enabled to true.
//addGlobalStyle('.column { transition: opacity ease-in-out var(--transition-time-in); }'); // transition vers le visible
//addGlobalStyle('.drawer:hover ~ .column { transition: opacity ease-in-out var(--transition-time-out); opacity: var(--opacity-hidden); }');

//addGlobalStyle('.drawer:hover ~ .column:has(.scrollable > div > .detailed-status__wrapper.focusable) { opacity: 1;}'); // garder la colonne d'un pouet sélectionné visible
//addGlobalStyle('.drawer:hover ~ .column:has(.scrollable > div.item-list > .account-timeline__header) { opacity: 1;}'); // garder la colonne d'un profil sélectionné visible


